<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'insights');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'A[TzJ1KH$|Ki}+VHTuz]2%.]=O_*EWhmi|T!<E-Y92cR9/r kBUkk6+*y*/twA5P');
define('SECURE_AUTH_KEY',  'QN<_?QA*})6&E1+Z|BD+J~N|_5;w9m||_GXxEeFY#e X1+K^-Xq a]}>, dJ8M#-');
define('LOGGED_IN_KEY',    'VDq T(5--|9eKeq5ey>~RW@H}<m(q)1Y,+EJB$qVvH}GOJim8Rz}Lu^9{Bh,:M1K');
define('NONCE_KEY',        '?ci+Aw(7rZ>q4INLD~-sv`>+p;wNBik~xj+/K3gCu+INzB=~]n,Y+ZV|12/jqp|J');
define('AUTH_SALT',        '&2o9O2M&HX6Ie-vvO(E$Gb|-|71_#On|%1[-J7l73~>1pfRmi?eb_9^-uVO6[$m;');
define('SECURE_AUTH_SALT', 'o|]iWB8de1MgfVK4|BUwg7W{9xEN^+x% 8Z;.u,~)| CI89$Bt0EUh*1J.K^85zQ');
define('LOGGED_IN_SALT',   'fKXhQt+$=!x (NkqN<W9rra-s,Kqu+WLTsW7pzGQev|c6d}-KPVNsIn4|No+nP#R');
define('NONCE_SALT',       'R]D2^b4 T]T=(=rrNI(W95)*.R-:64^De(Y{(hdw;4g&F/*PvCwPM{-z~#fQJpOb');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
