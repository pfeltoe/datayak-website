<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
	exit;
}

/* Template Name:  404 Error */

get_header(); ?>
            
            
            
            <h1 class="register-title">Oh shoot! Something went wrong.</h1>
            
            <img class=" size-medium wp-image-219 aligncenter" src="<?php echo home_url(); ?>/wp-content/uploads/2015/03/yak-300x153.png" alt="yak" width="300" height="153" />

            <p>Either this page no longer exists, or our bovine pals are off to pasture.</p>
                    
            <a href="<?php echo home_url(); ?>" class="homelink">Return Home</a>
  

<?php get_footer(); ?>
