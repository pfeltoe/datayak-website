<?php

	// Exit if accessed directly
	if ( !defined( 'ABSPATH' ) ) {
		exit;
	}

	/**
	* Blog Template
	*
	Template Name: Blog Excerpt (summary)
	*
	* @file           blog-excerpt.php
	* @package        Responsive
	* @author         Emil Uzelac
	* @copyright      2003 - 2014 CyberChimps
	* @license        license.txt
	* @version        Release: 1.1.0
	* @filesource     wp-content/themes/responsive/blog-excerpt.php
	* @link           http://codex.wordpress.org/Templates
	* @since          available since Release 1.0
	*/

	get_header();
?>




<div class="whole-wrapper">
	<div id="content-blog">

		<?php get_template_part( 'loop-header', get_post_type() ); ?>


		<?php
			global $wp_query, $paged;
			if ( get_query_var( 'paged' ) ) {
				$paged = get_query_var( 'paged' );
			}elseif ( get_query_var( 'page' ) ) {
				$paged = get_query_var( 'page' );
			}
			else {
				$paged = 1;
			}
			$blog_query = new WP_Query( array( 'post_type' => 'post', 'paged' => $paged ) );
			$temp_query = $wp_query;
			$wp_query = null;
			$wp_query = $blog_query;

			if ( $blog_query->have_posts() ) :

				while( $blog_query->have_posts() ) : $blog_query->the_post();
		?>

		<?php responsive_entry_before(); ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>



			<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>

			<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="one featured-image" style="background-image: url('<?php echo $thumb['0'];?>')">
			</a>

			<div class="two wrap">
				<h1 class="entry-title post-title"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h1>
				<h4><?php the_field('subtitle'); ?></h4>
 				
				<p class="p-meta">Posted in <?php the_category( ' ' ); ?> by <?php the_author_posts_link(); ?> on <?php the_time('F j'); ?> </p>
			</div>

			<?php responsive_entry_bottom(); ?>
		</div>
		<?php responsive_entry_after(); ?>

		<?php
			endwhile;

			if ( $wp_query->max_num_pages > 1 ) :
				?>
			<div class="navigation">
				<div class="previous"><?php next_posts_link( __( '&#8249; Older posts', 'responsive' ), $wp_query->max_num_pages ); ?></div>
				<div class="next"><?php previous_posts_link( __( 'Newer posts &#8250;', 'responsive' ), $wp_query->max_num_pages ); ?></div>
			</div>
			<?php
			endif;

			else :

				get_template_part( 'loop-no-posts', get_post_type() );

			endif;
			$wp_query = $temp_query;
			wp_reset_postdata();
		?>

	</div>
</div>
<?php get_footer(); ?>
