<div id="hero">
	<div>
		<span>Growth for e-commerce</span>
		<h1>You have data, we have answers. </h1>
		<p>See what’s working, what’s not, opportunities for optimization and get customized actions on what to do next. </p>
		<a href="#getstarted">Start Your 14-Day Free Trial</a>

	</div>
</div>

<div class="grid col-940 feat-audit">
	<p>Want better marketing ROI? Get your site analyzed for free.</p>
	<form accept-charset="UTF-8" action="https://formkeep.com/f/d9cbaf9eb143" method="POST" id="form-newsletter">
		<input type="hidden" name="utf8" value="?">
		<input type="email" id="email" name="email" placeholder="audit@datayak.com" data-validetta="required,email">
		<input type="submit" value="Get Your Free Audit">
	</form>
</div>