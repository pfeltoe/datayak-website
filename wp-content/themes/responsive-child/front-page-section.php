<div class="grid col-940 feat-testimonials">

	<!-- 	<a href="#" class="unslider-arrow prev"><i class="fa fa-chevron-left"></i></a>
	<div class="slider">
		<ul>
			<li> 
				<p>"Immediately helpful! Datayak made it easy for us to understand where are most valuable customers were coming from and how to go after them."</p>
				<div class="who-wrapper">
					<img src="http://datayak.co/wp-content/uploads/2015/04/donny.jpg">
					<h4>Donny <a href="http://rayku.com/home" target="_blank">Founder, Rayku</a></h4>
				</div>
			</li>
			<li>
				<p>"This is the tool I was waiting for. It saves me hours every month and uncovers incredibly valuable metrics and insights."</p>
				<div class="who-wrapper">
					<img src="http://datayak.co/wp-content/uploads/2015/04/anton.jpg">
					<h4>Anton <a href="http://wildround.com/" target="_blank">Founder, Wildround Growth Agency</a></h4>
				</div>
			</li>
		</ul>
	</div>
	<a href="#" class="unslider-arrow next"><i class="fa fa-chevron-right"></i></a> -->

	<p>Join these and 400 other companies in improving marketing ROI</p>

	<ul>
		<li><a target="_blank" href="http://wildround.com"><img class="" alt="Wildround Company Logo" src="<?php echo get_stylesheet_directory_uri(); ?>/img/clients/client_wildround.svg"></a></li>
		<li><a target="_blank" href="http://rayku.com"><img class="" alt="Rayku Company Logo" src="<?php echo get_stylesheet_directory_uri(); ?>/img/clients/client_rayku.svg"></a></li>
		<li><a target="_blank" href="http://doorstepdelivery.com/"><img class="" alt="Doorstep Delivery Company Logo" src="<?php echo get_stylesheet_directory_uri(); ?>/img/clients/client_doorstep.svg"></a></li>
		<li><a target="_blank" href="http://www.growthgeeks.com"><img class="" alt="GrowthGeeks Company Logo" src="<?php echo get_stylesheet_directory_uri(); ?>/img/clients/client_growthgeeks.svg"></a></li>
	</ul>

</div>

<div class="grid col-940 feat-benefits">

	<div class="benefit">
		<div class="half one">
			
			<h4><img class="benefit-icon" alt="Whats Working Section Icon" src="<?php echo get_stylesheet_directory_uri(); ?>/img/benefits/icons-01.svg"> What’s working?</h4>
			<p>Pinpoint the campaigns, products and actions that are driving revenue. </p>
		</div>
		<div class="half two"><img class="screenshot" alt="Whats Working Section Image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/screenshots/working.png"></div>
	</div>
	<div class="benefit">
		<div class="half one">
			
			<h4><img class="benefit-icon" alt="Whats Broken Section Icon" src="<?php echo get_stylesheet_directory_uri(); ?>/img/benefits/icons-02.svg">What’s broken?</h4>
			<p>Clearly understand the campaigns, products and actions that aren’t doing so well and how to fix it.</p>
		</div>
		<div class="half two"><img class="screenshot" alt="Whats Broken Section Image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/screenshots/broken.png"></div>
	</div>
	<div class="benefit">
		<div class="half one">
			<h4><img class="benefit-icon" alt="Opportunities Section Icon" src="<?php echo get_stylesheet_directory_uri(); ?>/img/benefits/icons-03.svg">Opportunities</h4>
			<p>See optimization, lost revenue, and channel opportunities. </p>
		</div>
		<div class="half two"><img class="screenshot" alt="Opportunities Section Image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/screenshots/funnel.png"></div>
	</div>
	<div class="benefit">
		<div class="half one">
			
			<h4><img class="benefit-icon" alt="Whats Next Section Icon" src="<?php echo get_stylesheet_directory_uri(); ?>/img/benefits/icons-04.svg">What's next?</h4>
			<p>Intelligent recommendations on what to do next based on your business, data and goals - done by 100% human growth specialists.</p>
		</div>
		<div class="half two"><img class="screenshot" alt="Whats Next Section Image" src="<?php echo get_stylesheet_directory_uri(); ?>/img/screenshots/support.png"></div>
	</div>
</div>

<div class="grid col-940 feat-seepricing">
	<p>Ready to get more from your analytics?</p>
	<a href="#getstarted">Start your 14-Day Free Trial</a>
</div>

<div class="grid col-940 feat-integrations">

	<div class="wrapper"><h2>Integrations</h2>
		<p>We’ve done all the work so you don’t have to. With countless integrations you are able to analyze cross channel data with ease.</p>
	
		<ul>
			<li>
				<a href="http://google.com/analytics" target="_blank">
					<img class="integ" alt="Google Analytics Logo" src="<?php echo get_stylesheet_directory_uri(); ?>/img/integrations/integrations-googleanalytics.svg">
				</a>
			</li>
			<li>
				<a href="http://google.com/adwords" target="_blank">
					<img class="integ" alt="Google AdWords Logo" src="<?php echo get_stylesheet_directory_uri(); ?>/img/integrations/integrations-googleaw.svg">
				</a>
			</li>
			<li>
				<a href="http://facebook.com" target="_blank">
					<img class="integ" alt="Facebook Logo" src="<?php echo get_stylesheet_directory_uri(); ?>/img/integrations/integrations-facebook.svg">
				</a>
			</li>
			<li>
				<a href="http://twitter.com" target="_blank">
					<img class="integ" alt="Twitter Logo" src="<?php echo get_stylesheet_directory_uri(); ?>/img/integrations/integrations-twitter.svg">
				</a>
			</li>
	
		</ul>
	
		<h3>Coming Soon</h3>
		<ul class="coming-soon">
			<li>
				<a href="http://mailchimp.com" target="_blank">
					<img class="integ" alt="Mailchimp Logo" src="<?php echo get_stylesheet_directory_uri(); ?>/img/integrations/integrations-mailchimp.svg">
				</a>
			</li>
			<li>
				<a href="http://woocommerce.com" target="_blank">
					<img class="integ" alt="WooCommerce Logo" src="<?php echo get_stylesheet_directory_uri(); ?>/img/integrations/integrations-woocommerce.svg">
				</a>
			</li>
			<li>
				<a href="http://instagram.com" target="_blank">
					<img class="integ" alt="Instagram Logo" src="<?php echo get_stylesheet_directory_uri(); ?>/img/integrations/integrations-instagram.svg">
				</a>
			</li>
			<li>
				<a href="http://stripe.com" target="_blank">
					<img class="integ" alt="Stripe Logo" src="<?php echo get_stylesheet_directory_uri(); ?>/img/integrations/integrations-stripe.svg">
				</a>
			</li>
			<li>
				<a href="http://linkedin.com" target="_blank">
					<img class="integ" alt="LinkedInLogo " src="<?php echo get_stylesheet_directory_uri(); ?>/img/integrations/integrations-linkedin.svg">
				</a>
			</li>
			<li>
				<a href="http://kissmetrics.com" target="_blank">
					<img class="integ" alt="KISSMetrics Logo" src="<?php echo get_stylesheet_directory_uri(); ?>/img/integrations/integrations-kissmetrics.svg">
				</a>
			</li>
			<li>
				<a href="http://paypal.com" target="_blank">
					<img class="integ" alt="PayPal Logo" src="<?php echo get_stylesheet_directory_uri(); ?>/img/integrations/integrations-paypal.svg">
				</a>
			</li>
			<li>
				<a href="http://shopify.com" target="_blank">
					<img class="integ" alt="Shopify Logo" src="<?php echo get_stylesheet_directory_uri(); ?>/img/integrations/integrations-shopify.svg">
				</a>
			</li>
		</ul></div>
</div>





