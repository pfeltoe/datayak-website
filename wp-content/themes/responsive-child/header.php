<?php
/**
 * Header Template
 *
 *
 * @file           header.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2014 CyberChimps
 * @license        license.txt
 * @version        Release: 1.3
 * @filesource     wp-content/themes/responsive/header.php
 * @link           http://codex.wordpress.org/Theme_Development#Document_Head_.28header.php.29
 * @since          available since Release 1.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
	exit;
}

?>

<!doctype html>
	<!--[if !IE]>
	<html class="no-js non-ie" <?php language_attributes(); ?>> <![endif]-->
	<!--[if IE 7 ]>
	<html class="no-js ie7" <?php language_attributes(); ?>> <![endif]-->
	<!--[if IE 8 ]>
	<html class="no-js ie8" <?php language_attributes(); ?>> <![endif]-->
	<!--[if IE 9 ]>
	<html class="no-js ie9" <?php language_attributes(); ?>> <![endif]-->
	<!--[if gt IE 9]><!-->

<html class="no-js" <?php language_attributes(); ?>> 

	<head>

		<meta charset="<?php bloginfo( 'charset' ); ?>"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="theme-color" content="#EC5D47">

		<meta name="application-name" content="Datayak"/>
		<meta name="msapplication-TileColor" content="#EC5D47"/>
		<meta name="msapplication-square70x70logo" content="http://datayak.co/wp-content/uploads/2015/06/windows-tiny.png"/>
		<meta name="msapplication-square150x150logo" content="http://datayak.co/wp-content/uploads/2015/06/windows-square.png"/>
		<meta name="msapplication-wide310x150logo" content="http://datayak.co/wp-content/uploads/2015/06/windows-wide.png"/>
		<meta name="msapplication-square310x310logo" content="http://datayak.co/wp-content/uploads/2015/06/windows-large.png"/>
		<meta name="msapplication-notification" content="frequency=30;polling-uri=http://notifications.buildmypinnedsite.com/?feed=http://datayak.co/feed/&amp;id=1;polling-uri2=http://notifications.buildmypinnedsite.com/?feed=http://datayak.co/feed/&amp;id=2;polling-uri3=http://notifications.buildmypinnedsite.com/?feed=http://datayak.co/feed/&amp;id=3;polling-uri4=http://notifications.buildmypinnedsite.com/?feed=http://datayak.co/feed/&amp;id=4;polling-uri5=http://notifications.buildmypinnedsite.com/?feed=http://datayak.co/feed/&amp;id=5; cycle=1"/>

		<title><?php wp_title( '&#124;', true, 'right' ); ?></title>

		<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />
		<link rel="profile" href="http://gmpg.org/xfn/11"/>
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>"/>
		
		<?php wp_head(); ?>

	</head>

	<body <?php body_class(); ?>>

		<!-- Google Tag Manager -->
			<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-TBTDX7"
			height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
			<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
			new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
			j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
			'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
			})(window,document,'script','dataLayer','GTM-TBTDX7');</script>
		<!-- End Google Tag Manager -->
		
		<header>
			<a class="logo_wrapper" href="<?php echo home_url(); ?>">
				<img class="logo" alt="Datayak Logo" src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.svg">
				<h1>Data<span>yak</span></h1>
			</a>
			<?php wp_nav_menu( array( 'theme_location' => 'top-menu' ) ); ?>
		</header>

		<div class="remodal" data-remodal-id="getstarted">
	        
	        <h5>Start your 14-Day Free Trial</h5>
	        <hr>
			
			<form novalidate autocomplete="on" accept-charset="UTF-8" action="https://formkeep.com/f/1ebd2e9a19ad" method="POST" id="form-register">
				<div class="form-group grid col-460">
					<label for="cc-name" class="control-label">Full Name</label>
					<input id="cc-name" type="text" placeholder="Jane Smith" name="name" required data-validetta="required">
				</div>

				<div class="form-group grid col-460 fit">
					<label for="cc-phone" class="control-label">Phone</label>
					<input id="cc-phone" type="tel" placeholder="(650) 451 9882" name="phone" data-mask="(000) 000 0000" required data-numeric data-validetta="required">
				</div>

				<div class="form-group grid col-940">
					<label for="cc-email" class="control-label">Email</label>
					<input id="cc-email" type="email" placeholder="jane@company.com" name="email" required data-validetta="required,email">
				</div>

				<input id="pricingplan" type="hidden" name="pricingplan" value="0">

				<input type="hidden" name="utf8" value="✓">
				<button type="submit" class="btn btn-lg btn-primary grid col-940">Join Now</button>

			</form>
	    </div>

	    <div class="remodal refer-modal" data-remodal-id="thanks">

			<p>Thanks signing up for your free trial!</p>

			<p>Within the next 24 hours, we will contact you to get you setup with your trial account and a few questions to make sure we're the right fit for you and your project.</p> 

			<p>Look forward to speaking with you!</p>

			<hr>

		    <h5>Share us with your friends!</h5>
	        <div class="custom-social">
	            <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=datayak.co" class="grid col-300 facebook"><i class="fa fa-facebook"></i></a>
	            <a target="_blank" href="https://twitter.com/home?status=Marketing%20channel%20optimization%20made%20easy.%20Check%20out%20http://datayak.co" class="grid col-300 twitter"><i class="fa fa-twitter"></i></a>
	            <a target="_blank" href="mailto:friends@datayak.co?&subject=Check out datayak.co&body=Marketing%20channel%20optimization%20made%20easy" class="grid col-300 fit email"><i class="fa fa-envelope"></i></a>
	        </div>
	    </div>

	    <div class="remodal" data-remodal-id="question">
	    	<h3>Have any questions we can answer?</h3>
	    	<form novalidate autocomplete="on" accept-charset="UTF-8" action="https://formkeep.com/f/c7e19490564e" method="POST" id="form-question">
				<div class="form-group grid col-940">
					<label for="cc-email" class="control-label">Email</label>
					<input id="cc-email" type="email" placeholder="jane@company.com" name="email" required data-validetta="required,email">
				</div>

				<div class="form-group grid col-940">
					<label for="cc-question" class="control-label">Question</label>
					<input id="cc-question" type="textarea" placeholder="How does Datyak help me with..." name="text" required data-validetta="required">
				</div>

				<input type="hidden" name="utf8" value="✓">
				<button type="submit" class="btn btn-lg btn-primary grid col-940">Ask an Expert</button>

			</form>
	    </div>
			
		<?php responsive_container(); // before container hook ?>
		<div id="container" class="hfeed">
			<?php responsive_wrapper(); // before wrapper container hook ?>
			<div id="wrapper" class="clearfix">
				<?php responsive_wrapper_top(); // before wrapper content hook ?>
				<?php responsive_in_wrapper(); // wrapper hook ?>
