jQuery(document).ready(function($) {

    // var unslider = $('.slider').unslider({
    // 	speed: 500,               //  The speed to animate each slide (in milliseconds)
    // 	delay: 5000,              //  The delay between slide animations (in milliseconds)
    // 	keys: false,               //  Enable keyboard (left, right) arrow shortcuts
    // 	dots: false,               //  Display dot navigation
    // 	fluid: true              //  Support responsive design. May break non-responsive designs
    // });;

    // $('.unslider-arrow').click(function() {
    //     var fn = this.className.split(' ')[1];
    //     //  Either do unslider.data('unslider').next() or .prev() depending on the className
    //     unslider.data('unslider')[fn]();
    // });

    $("#form-register").validetta();
    $("#form-emailonly").validetta();
    $("#form-newsletter").validetta();
    $("#form-question").validetta();

    $(".feat-audit span").click(function() {
        $(".feat-audit .description").slideToggle("slow", function() {});
    });

    $('a.sign-up').click(function() {
        var pricingplan = $(this).attr('data-pricingplan');
        $('#pricingplan').val(pricingplan);
    });


    function openModal(){
        var inst = $('[data-remodal-id=question]').remodal();
        inst.open();
    }
    timer = setTimeout(openModal, 60000);

    $('.question a').click(function() {
        clearTimeout(timer);
    });
    
});