<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Single Posts Template
 *
 *
 * @file           single.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2014 CyberChimps
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/single.php
 * @link           http://codex.wordpress.org/Theme_Development#Single_Post_.28single.php.29
 * @since          available since Release 1.0
 */

get_header(); ?>


<div class="whole-wrapper">
<div id="content">

	<?php get_template_part( 'loop-header', get_post_type() ); ?>

	<?php if ( have_posts() ) : ?>

		<?php while( have_posts() ) : the_post(); ?>

			<?php responsive_entry_before(); ?>
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>

				<div class="featured-image-single" style="background-image: url('<?php echo $thumb['0'];?>')">
				</div>
						 
			 			
			    <div class="wrap">
        			<h1 class="entry-title post-title"><?php the_title(); ?></h1>
					<h4><?php the_field('subtitle'); ?></h4>
					<p class="p-meta">Posted in <?php the_category( ' ' ); ?> by <?php the_author_posts_link(); ?> on <?php the_time('F j'); ?> </p>
        		</div>

        		

				<div class="post-entry">
					<?php the_content( __( 'Read more &#8250;', 'responsive' ) ); ?>
					<?php responsive_comments_before(); ?>
					<?php comments_template( '', true ); ?>
					<?php responsive_comments_after(); ?>
				</div>



				<?php responsive_entry_bottom(); ?>
			</div>
			<?php responsive_entry_after(); ?>

			<div class="navigation">
					<div class="previous"><?php previous_post_link( '&#8249; %link' ); ?></div>
					<div class="next"><?php next_post_link( '%link &#8250;' ); ?></div>
			</div><!-- end of .navigation -->

		<?php
		endwhile;

		get_template_part( 'loop-nav', get_post_type() );

	else :

		get_template_part( 'loop-no-posts', get_post_type() );

	endif;
	?>

</div><!-- end of #content -->
</div>
<?php get_footer(); ?>
