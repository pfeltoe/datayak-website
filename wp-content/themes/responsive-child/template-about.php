<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
	exit;
}

/* Template Name:  Yak - About */

get_header(); ?>


<div id="content">

	<section class="introduction">

		<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/icons/work.svg"></a></li>

		<h1>We come to work every day because we want to solve the biggest problems in analytics</h1>

		<p>Every company has data but it takes too much time and way too many excel sheets to figure out what do to with it. Our mission is make your data accessible and insightful so you can make informed decisions and grow your business. Today we work with hundreds of companies making their data useful and helping them grow their companies.</p> 
	</section>
	<section class="faces">
		<section>
			<img class="introicon" src="http://datayak.co/wp-content/uploads/2015/08/em_amanda.jpg">
			<h1>Amanda Parker</h1>
			<h2>Co-Founder &amp; CEO</h2>
			<ul class="social">
				<li class="twitter"><a href="https://twitter.com/uncannymandy" target="_blank"><i class="fa fa-twitter"></i></a></li>
				<li class="linkedin"><a href="http://ca.linkedin.com/in/parkeramanda" target="_blank"><i class="fa fa-linkedin"></i></a></li>
				<li class="angellist"><a href="https://angel.co/amanda-parker" target="_blank"><i class="fa fa-angellist"></i></a></li>
			</ul>
		</section>
		<section>
			<img class="introicon" src="http://datayak.co/wp-content/uploads/2015/08/em_alexey.jpg">
			<h1>Alexey Adamsky</h1>
			<h2>Co-Founder &amp; CIO</h2>
			<ul class="social">
				<li class="twitter"><a href="https://twitter.com/alkasai" target="_blank"><i class="fa fa-twitter"></i></a></li>
				<li class="linkedin"><a href="http://ca.linkedin.com/in/alkasai" target="_blank"><i class="fa fa-linkedin"></i></a></li>
				<li class="angellist"><a href="https://angel.co/alkasai" target="_blank"><i class="fa fa-angellist"></i></a></li>
			</ul>
		</section>
		<section>
			<img class="introicon" src="http://datayak.co/wp-content/uploads/2015/08/em_kowsheek.png">
			<h1>Kowsheek Mahmood</h1>
			<h2>CTO</h2>
			<ul class="social">
				<li class="twitter"><a href="https://twitter.com/aredkid" target="_blank"><i class="fa fa-twitter"></i></a></li>
				<li class="linkedin"><a href="http://ca.linkedin.com/in/kowsheek" target="_blank"><i class="fa fa-linkedin"></i></a></li>
				<li class="angellist"><a href="https://angel.co/aredkid" target="_blank"><i class="fa fa-angellist"></i></a></li>
			</ul>
		</section>
		<section>
			<img class="introicon" src="http://datayak.co/wp-content/uploads/2015/08/em_paul.jpg">
			<h1>Paul Feltoe</h1>
			<h2>CDO</h2>
			<ul class="social">
				<li class="twitter"><a href="https://twitter.com/paulfeltoe" target="_blank"><i class="fa fa-twitter"></i></a></li>
				<li class="linkedin"><a href="http://ca.linkedin.com/in/paulfeltoe" target="_blank"><i class="fa fa-linkedin"></i></a></li>
				<li class="angellist"><a href="https://angel.co/paulfeltoe" target="_blank"><i class="fa fa-angellist"></i></a></li>
			</ul>
		</section>
		<!-- <div>
			<img class="introicon" src="http://datayak.co/wp-content/uploads/2015/08/em_slava.png">
			<h1>Slava Pocheptsov</h1>
			<h2>CDO</h2>
			<ul class="social">
				<li class="twitter"><a href="https://twitter.com/pocheptsov" target="_blank">T</a></li>
				<li class="linkedin"><a href="http://ca.linkedin.com/in/vpocheptsov" target="_blank">L</a></li>
				<li class="angellist"><a href="https://angel.co/slava-pocheptsov" target="_blank">A</a></li>
			</ul>
		</div> -->
	</section>
	<section class="about-question">
		<p>We’d love to talk with you. Feel free to ask us a question!</p>

		<form novalidate autocomplete="on" accept-charset="UTF-8" action="https://formkeep.com/f/c7e19490564e" method="POST" id="form-question">
			<div class="form-group grid col-940">
				<label for="cc-email" class="control-label">Email</label>
				<input id="cc-email" type="email" placeholder="jane@company.com" name="email" required data-validetta="required,email">
			</div>

			<div class="form-group grid col-940">
				<label for="cc-question" class="control-label">Question</label>
				<input id="cc-question" type="textarea" placeholder="How does Datyak help me with..." name="text" required data-validetta="required">
			</div>

			<input type="hidden" name="utf8" value="✓">
			<button type="submit" class="btn btn-lg btn-primary grid col-940">Ask an Expert</button>

		</form>

	</section>

</div>

<?php get_footer(); ?>
