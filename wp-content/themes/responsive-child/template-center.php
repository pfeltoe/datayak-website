<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
	exit;
}

/* Template Name:  Yak - Center */

get_header(); ?>


<div class="whole-wrapper">
	<div id="content">

	<?php get_template_part( 'loop-header', get_post_type() ); ?>

	<?php if ( have_posts() ) : ?>

		<?php while( have_posts() ) : the_post(); ?>

			<?php responsive_entry_before(); ?>
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>

				<div class="featured-image-single" style="background-image: url('<?php echo $thumb['0'];?>')">
				</div> 
			 			
			    <div class="wrap">
        			<h1 class="entry-title post-title"><?php the_title(); ?></h1>
        			<hr>
        		</div>
        		

				<div class="post-entry">
					<?php the_content( __( 'Read more &#8250;', 'responsive' ) ); ?>
				</div>



				<?php responsive_entry_bottom(); ?>
			</div>
			<?php responsive_entry_after(); ?>

		<?php
		endwhile;

		get_template_part( 'loop-nav', get_post_type() );

	else :

		get_template_part( 'loop-no-posts', get_post_type() );

	endif;
	?>

</div><!-- end of #content -->
</div>
<?php get_footer(); ?>
