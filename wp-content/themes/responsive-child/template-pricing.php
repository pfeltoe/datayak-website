<!-- Header -->
<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
	exit;
}

/* Template Name:  Yak - Pricing Page */
get_header(); ?>

<div class="grid col-940 feat-freetrial">
    <p>All plans include a 14-Day Free Trial</p>
</div>

<div class="grid col-940 feat-getearlyaccess">
    <table>
        <thead>
            <tr>
                <th></th>
                <th>
                    <h1>Free</h1>
                </th>
                <th>
                    <h1>Optimize</h1>
                </th>
                <th>
                    <h1>Grow</h1>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Metrics Dashboard</td>
                <td><i class="fa fa-check fa-lg"></i></td>
                <td><i class="fa fa-check fa-lg"></i></td>
                <td><i class="fa fa-check fa-lg"></i></td>
            </tr>
            <tr>
                <td>Cohort Comparison</td>
                <td><i class="fa fa-check fa-lg"></i></td>
                <td><i class="fa fa-check fa-lg"></i></td>
                <td><i class="fa fa-check fa-lg"></i></td>
            </tr>
            <tr>
                <td>Access to Growth Executors</td>
                <td><i class="fa fa-check fa-lg"></i></td>
                <td><i class="fa fa-check fa-lg"></i></td>
                <td><i class="fa fa-check fa-lg"></i></td>
            </tr>
            <tr>
                <td>Custom Reports</td>
                <td><i class="fa fa-times"></i></td>
                <td><i class="fa fa-check fa-lg"></i></td>
                <td><i class="fa fa-check fa-lg"></i></td>
            </tr>
            <tr>
                <td>Growth & Optimization Engine</td>
                <td><i class="fa fa-times"></i></td>
                <td><i class="fa fa-check fa-lg"></i></td>
                <td><i class="fa fa-check fa-lg"></i></td>
            </tr>
            <tr>
                <td>Data Alerts</td>
                <td><i class="fa fa-times"></i></td>
                <td><i class="fa fa-check fa-lg"></i></td>
                <td><i class="fa fa-check fa-lg"></i></td>
            </tr>
            <tr>
                <td>Weekly Recommendations</td>
                <td><i class="fa fa-times"></i></td>
                <td><i class="fa fa-check fa-lg"></i></td>
                <td><i class="fa fa-check fa-lg"></i></td>
            </tr>
            <tr>
                <td>Dedicated Growth Team</td>
                <td><i class="fa fa-times"></i></td>
                <td><i class="fa fa-times"></i></td>
                <td><i class="fa fa-check fa-lg"></i></td>
            </tr>
            <tr>
                <td>Custom Integrations</td>
                <td><i class="fa fa-times"></i></td>
                <td><i class="fa fa-times"></i></td>
                <td><i class="fa fa-check fa-lg"></i></td>
            </tr>
            <tr class="pricing-row">
                <td></td>
                <td>
                    <h3>$0</h3>
                    <p> per month</p><a href="#getstarted" data-pricingplan="0" class="sign-up">Sign Up</a></td>
                <td>
                    <h3>$500</h3>
                    <p> per month</p><a href="#getstarted" data-pricingplan="500" class="sign-up">Sign Up</a></td>
                <td>
                    <p>Call for Pricing</p>
                    <div class="call-us">1.877.629.5620</div>
                </td>
            </tr>
        </tbody>
    </table>
</div>

<div class="grid col-940 feat-faq">
    <section><div class="grid col-460">
            <h4>Can I cancel my account?</h4>
            <p>Yes. Although we would hate to see you go, you can easily cancel your account by messaging “cancel me” inside of the app’s chat.</p>
        </div>
        <div class="grid col-460 fit">
            <h4>Money back guarantee</h4>
            <p>We want you to be 100% satisfied. If you’re not, you cancel your service within the first 30-days for a full refund.</p>
        </div></section>
</div>

<!-- Footer -->
<?php get_footer(); ?>