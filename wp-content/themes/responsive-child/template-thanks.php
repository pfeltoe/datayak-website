<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
	exit;
}

/* Template Name:  Yak - Thanks */

get_header(); ?>
            
            
            <h1 class="register-title">Thanks for joining the newsletter!</h1>
            
            <img class=" size-medium wp-image-219 aligncenter" src="<?php echo home_url(); ?>/wp-content/uploads/2015/03/yak-300x153.png" alt="yak" width="300" height="153" />

            <p>We hope you enjoy the great content coming your way.</p>
                    
            <a href="<?php echo home_url(); ?>" class="homelink">Return Home</a>
  

<?php get_footer(); ?>
